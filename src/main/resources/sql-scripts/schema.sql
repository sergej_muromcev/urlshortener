CREATE TABLE uri_storage (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  uri varchar(1000) NOT NULL,
  lastAccessed timestamp not null default current_date,
  PRIMARY KEY (id)
);
