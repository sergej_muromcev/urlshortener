package lv.sm.test.urishortener.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "uri_storage")
@AllArgsConstructor
@NoArgsConstructor
public class UriEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "uri")
    private String uri;

    @Column(name = "lastAccessed")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime lastAccessed;

    public UriEntity(String uri) {
        this.uri = uri;
        lastAccessed = DateTime.now();
    }
}
