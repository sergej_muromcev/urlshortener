package lv.sm.test.urishortener.api.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ShortenedUri {
    private String key;
}
