package lv.sm.test.urishortener.repository;

import lv.sm.test.urishortener.domain.UriEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UriRepository extends CrudRepository<UriEntity, Long> {
    Optional<UriEntity> findById(long id);

}
