package lv.sm.test.urishortener.util;

import lombok.experimental.var;
import lombok.val;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class UriHelper {

    //randomizing alphabet is an option
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final Pattern REGEX = Pattern.compile("[" + ALPHABET + "]+");
    private static final int BASE = ALPHABET.length();

    public static long decode(String src) {
        Matcher matcher = REGEX.matcher(src);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("src is empty or contains invalid characters.");
        }
        var num = 0L;
        for (int i = 0; i < src.length(); i++)
            num = num * BASE + ALPHABET.indexOf(src.charAt(i));
        return num;
    }

    public static String encode(long id) {
        if (id < 1L) {
            throw new IllegalArgumentException("id should be positive non-null long.");
        }
        val sb = new StringBuilder();
        while (id > 0) {
            sb.append(ALPHABET.charAt((int) (id % BASE)));
            id /= BASE;
        }
        return sb.reverse().toString();
    }
}
