package lv.sm.test.urishortener.metrics;

import lombok.val;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class MeteredInerceptor {
    @Around("@annotation(lv.sm.test.urishortener.metrics.MeteredMethod)")
    public Object myAdvice(ProceedingJoinPoint proceedingJoinPoint) {
        val className = proceedingJoinPoint.getTarget().getClass().getName();
        val methodName = proceedingJoinPoint.getSignature().getName();

        Object value = null;
        try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }


        return value;
    }

}
