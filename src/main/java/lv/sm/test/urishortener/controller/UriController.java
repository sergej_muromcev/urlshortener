package lv.sm.test.urishortener.controller;

import lombok.NonNull;
import lombok.val;
import lv.sm.test.urishortener.api.response.ShortenedUri;
import lv.sm.test.urishortener.domain.UriEntity;
import lv.sm.test.urishortener.metrics.MeteredMethod;
import lv.sm.test.urishortener.repository.UriRepository;
import lv.sm.test.urishortener.util.UriHelper;
import org.apache.commons.validator.routines.UrlValidator;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.security.InvalidParameterException;

@RestController
public class UriController {

    @Autowired
    UriRepository uriRepository;

    /**
     * @param uri has to contain http/https protocol in order to be accepted by Apache UrlValidator
     * @return
     */
    @PostMapping("/shorten")
    @MeteredMethod
    public ShortenedUri shortenUri(@RequestParam(value = "uri") String uri) {
        val urlValidator = new UrlValidator();
        if (!urlValidator.isValid(uri)) {
            throw new InvalidParameterException("Provided Url is malformed. Sorry, we're not going to shorten it.");
        }

        val tempUri = new UriEntity(uri);
        val storedUri = uriRepository.save(tempUri);
        val shortened = UriHelper.encode(storedUri.getId());
        return new ShortenedUri(shortened);
    }

    @GetMapping("/restore/{shortenedUri}")
    @MeteredMethod
    public void restoreUri(HttpServletResponse httpServletResponse, @PathVariable("shortenedUri") @NonNull String shortenedUri) {
        val id = UriHelper.decode(shortenedUri);
        val uri = uriRepository.findById(id);
        if (!uri.isPresent()) {
            throw new InvalidParameterException("We can't find original URI you're looking for.");
        }
        val url = uri.get();

        httpServletResponse.setHeader("Location", url.getUri());
        httpServletResponse.setStatus(302);

        url.setLastAccessed(DateTime.now());
        uriRepository.save(url);
    }

}
