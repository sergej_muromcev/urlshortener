package lv.sm.test.urishortener.util;

import lombok.experimental.var;
import lombok.val;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class UriHelperTest {
    @Test
    public void combined() {
        val rand = new Random(System.currentTimeMillis());
        for (var i = 0; i < 99; i++) {
            val value = Math.abs(rand.nextLong());
            val encoded = UriHelper.encode(value);
            Assert.assertEquals(value, UriHelper.decode(encoded));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void encodingError() {
        UriHelper.encode(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void decodingEmptyError() {
        val res = UriHelper.decode("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void decodingInvalidError() {
        val res = UriHelper.decode("@$%*^#@");
    }
}